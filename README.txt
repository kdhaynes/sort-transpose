Sort by Transpositions
------------------------

The program sort_transpose.py (python3.3) takes an input file consisting of
lines of single, non-repeated numbers (input.txt), sorts these using
transpositions, and returns a single number indicating the least number
of tranpositions required to put the numbers in order.

The transpositions used in this program are identified by T(i,j,k) such
that 1 <= i <= j < k <= n where i, j, and k are indices and n is the
length of the list.  The transposition swaps the index intervals [i...j]
and [j+1....k].

Notes: To do this problem, I tried several different informed search
techniques, including A* search, IDA* search, and recursive best first
search.  Originally the A* search had memory problems, which I resolved
with three main improvements: - Optimized the transposition itself,
including not separating numbers already in a sequence as well as using
copy to perform the transposition after several different speed tests on
various methods - Used five different heuristics, setting the final
function h to the maximum of these.  I did not prove that these
heuristics always work (admissable); however, they did work on the test cases
provided as well as numerous cases I independently tried. - Used a
dictionary to do the sorting and storing of the frontier.  Since there
are only a limited number of path lengths with a high number of
duplicates, I found a dictionary to be much faster than other methods of
storing/sorting/inserting new states into the frontier.  The key of the
dictionary is the f cost, and stored in that element is a list of nodes that is
appended every time a new node is added, with the last one popped off
when checking the next states.  I found this technique to make quite an
improvement on the speed, as it eleminated one long list that became
rapidly too slow to insert and allowed for easy checking to get the next
state with the mimimum f cost.

Using these improvements, I found that the A* search gave the best
(fastest) performance in the end.

------------------------
Katherine Haynes
February 25, 2019