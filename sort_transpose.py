#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 21 07:19:15 2019

@author: kdhaynes
"""
import math, time

class Problem:
    # The problem is the original state (list) that
    #    needs to be sorted using transpositions.
    # The state is a list.
    def __init__(self, initial):
        self._initial = initial

    # Return the initial state.
    def get_initial(self):
        return self._initial
    
    # Test to see if the state is ordered.    
    # Requires an input state.
    # Returns True or False.    
    def goal_test(self, state):
        return all(state[i] <= state[i+1] \
                   for i in range(len(state)-1))    

    # Find first location in list not in order.
    # Requires an input state.
    # Returns an index.
    def ifirst(self, mylist):
        for i in range(len(mylist)-1):
            if (mylist[i] > i+1):
                return i
        return 0

    # Find first location after a run.
    # Requires an input state.
    # Returns an index.
    def irun(self, mylist):
        mystart = mylist[0]
        for i in range(len(mylist)):
            if (mylist[i] != i+mystart):
                return i
        return len(mylist)

    # Transpose the state.
    # Inputs: state and three indices (i,j,k)
    # Returns a new state.
    def transpose(self, state, i, j, k):
        newlist = state.copy()
        newlist[i:i+k-j] = state[j+1:k+1]
        newlist [i+k-j:k+1] = state[i:j+1]
        return newlist
    
    # Get the successors of the state.
    # Requires an input state.
    # Returns a list of states.
    def get_successors(self, state):
        successor = []
        for i in range(self.ifirst(state),len(state)-1):
            for j in range(i+self.irun(state[i:])-1,len(state)-1):
                for k in range(j+self.irun(state[j+1:]),len(state)):
                    successor.append(self.transpose(state,i,j,k))
        return successor


class Node:
    # Class to hold a search tree node.
    def __init__(self, state, cost=0):
        self._state = state
        self._g = cost
        self._f = cost + self.get_heuristic(state)
            
    # Returns the state.
    def get_state(self):
        return self._state

    # Returns the f cost.
    def get_f(self):
        return self._f
    
    # Returns the h (heuristic) cost.
    def get_h(self):
        return self.get_heuristic(self.get_state())
    
    # Returns the actual cost (path length).
    def get_cost(self):
        return self._g

    # Sets the node cost to a specified value,
    # and updates the f (total) cost.
    # Requires an integer.
    def set_cost(self, value):
        self._g = value
        self._f = self._g + self.get_heuristic(self.get_state())
        
    # Create heuristic cost using a combination
    # of five different estimations.
    def get_heuristic(self, state):
        # Number of single elements out of place.
        myhcost1 = 0
        for i in range(1,len(state)-1):
            if (state[i] != i+1):
                if (abs(state[i]-state[i-1]) > 1):
                    if (abs(state[i]-state[i+1]) > 1):
                        myhcost1+=1
                        
        # Number of groups already on the correct half.
        myhcost2 = 0
        myhcost3 = 0
        nl = int(len(state)/2)
        for i in range(nl):
            if (state[i] < nl) and (abs(state[i+1]-state[i]) > 1):
                myhcost2+=1
        for i in range(nl,len(state)-1):
            if (state[i] > nl) and (abs(state[i+1]-state[i]) > 1):
                myhcost3+=1

        # Number of groups to transpose.
        myhcost4 = 0
        for i in range(len(state)-1):
            if abs(state[i]-state[i+1]) > 1:
                myhcost4 += 1
        myhcost4 = int(myhcost4/2)
        if (state[-1] == len(state)):
            myhcost4 -= 1

        # Number of groups compared to length of state.
        myhcost5 = 0
        if  abs(state[0]-state[1]) < 2:
            myhcost5 = 1
        for i in range(1,len(state)-1):
            if ((abs(state[i]-state[i+1]) < 2) or
                (abs(state[i]-state[i-1]) < 2) or
                (state[i] == i+1)):
                myhcost5 += 1
        myhcost5 = len(state) - myhcost5 - 1
        #print("COSTS:",myhcost1,myhcost2,myhcost3,myhcost4,myhcost5)
        #sys.exit()
        
        return max(myhcost1,myhcost2,myhcost3,myhcost4,myhcost5)

    # Expand the search node.
    # Requires a problem as input.
    # Returns a list of nodes.
    def expand(self, problem):
        childNodes = []
        
        cost = self.get_cost()+1
        currentState = self.get_state()
        newStates = problem.get_successors(currentState)
        for nstate in newStates:
            newNode = Node(nstate, cost)
            childNodes.append(newNode)
        return childNodes

#==============================================================================    
# Search for the number of transitions using A*.
# Requires a problem as input.
# Returns the result and an integer.
def astar_search(problem):
    explored = []
    frontier = {}
    
    node = Node(problem.get_initial())
    frontier[node.get_f()] = [node]
    while frontier:
        lowkey = min(frontier.keys())
        allnodes = frontier[lowkey]
        node = allnodes.pop()
        if (allnodes):
            frontier[lowkey] = allnodes
        else:
            del frontier[lowkey]
        
        if problem.goal_test(node.get_state()):
            return (node.get_cost(),-1)
        if node.get_state() not in explored:
            explored.append(node.get_state())
            successors = node.expand(problem)
            for s in successors:
                if (s.get_f() in frontier):
                    mylist = frontier[s.get_f()]
                    mylist.append(s)
                    frontier[s.get_f()] = mylist
                else:
                    frontier[s.get_f()] = [s]

    return (None, -1)


# Search for the number of transitions using IDA*.
# Requires a problem as input.
# Returns the result and an integer.
def idastar_search(problem):
    node = Node(problem.get_initial())
    flimit = node.get_f()
    
    while True:
        result,flimit = dfs_contour(problem,node,flimit)
        if result:
            return (result,-1)
        if (flimit == math.inf):
            return (None, -1)
    
def dfs_contour(problem,node,flimit):
    if (node.get_f() > flimit):
        return (None, node.get_f())
    if (problem.goal_test(node.get_state())):
        return (node.get_cost(), -1)
    
    successors = node.expand(problem)
    for s in successors:
        result,flimitnew = dfs_contour(problem,s,flimit)
        if result:
            return (s.get_f(),flimitnew)
        nextf = min(s.get_f(),flimitnew)
    return (None, nextf)

    
# Search for the number of transitions using RBFS.
# Requires a problem as input.
# Returns the result and an integer.
def recursive_best_first_search(problem):
    def RBFS(problem, node, flimit):
        if problem.goal_test(node.get_state()):
            return (node.get_cost(),-1)

        successors = node.expand(problem)
        if not successors:
            return (None, math.inf)

        while True:
            fcost = []
            for s in range(len(successors)):
                fcost.append(successors[s].get_f())
            mymin = min(fcost)
            myref = fcost.index(mymin)
            bestNode = successors[myref]
            if bestNode.get_f() > flimit:
                return (None, bestNode.get_f())
            
            fcost.remove(mymin)
            mymin2 = min(fcost)
            altflim = mymin2
            result, newf = RBFS(problem, bestNode, min(flimit, altflim))
            newcost = newf - bestNode.get_h()
            bestNode.set_cost(newcost)
            if result is not None:
                return (result, -1)
        
    return RBFS(problem, Node(problem.get_initial()), math.inf)

#==============================================================================
# Read in the input file:
filename='input.txt'
mylist=[]
with open(filename,'r') as f:
    for line in f:
        mylist.append(int(line))
#print ("")
#print ("Input File: ",filename)
myp = Problem(mylist)

# Find the number of transitions required 
#   to sort the list.
t0=time.time()

# Use A* Search
frontier = {}
total_trans = astar_search(myp)

# Use IDA* Search
#total_trans = idastar_search(myp)

# Use RBSF
#total_trans = recursive_best_first_search(myp)[0]

# Print Results
#result=time.time()-t0
#print ("Transitions Required: ",total_trans[0])
#print ("Time Taken (s): ",'{:.5f}'.format(result))
print (total_trans[0])